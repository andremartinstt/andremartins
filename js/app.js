var $select = $('#result'); // id da div onde será apresentado as notícias

$.getJSON('news.JSON', function(data){
	$select.html('');

	// Percorre o arquivo JSON e recupera cada dado, de acordo com sua posição
	for(var i = 0; i < data['news'].length; i++){
		
		// É retornado as informações do arquivo JSON, com a formatação do HTML
		$select.append("<article class='post'><a href='#' class='thumb'><img class='img-thumbnail' src='" + data['news'][i]['imagem'] + "'></a><div class='noticia'><h2 class='post-title'><a href='#'>" + data['news'][i]['titulo'] +  "</a></h2> <p class='post-conteudo'>" + data['news'][i]['descricao'] + "</p> <p class='post-data'><span>" + data['news'][i]['data'] + "</span></p></div> </article>");
		
	}
});