<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Notícias</title>
	<link rel="shortcut icon" href="img\icons\shortcut.png">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<section class="container">
		
		<!-- LOGO -->
		<div class="row">
			<section class="col-md-9 col-md-push-1 Busca-01">
				<img src="img/bitmap.jpg">
			</section>
		</div>

		<!-- BARRA DE PESQUISA -->
		<div class="row">
			<section class="col-md-9 col-md-push-1 box-1">				
				<div class="navbar-header">
					<div class="collapse navbar-collapse" id="navegacion-fm">
						<div class="form-group">			
							<div class="input-group input-group-lg">
								<div class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-search"></span></div>
								<input type="text" class="form-control" aria-describedby="basic-addon1">
								<div class="input-group-btn">
									<button type="button" class="btn btn-default btn-todas dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Todas <span class="caret"></span>
									</button>
									<ul class="dropdown-menu">
										<li><a href="#">Notícias</a></li>
										<li><a href="#">Fotos</a></li>
									</ul>
									<button type="button" class="btn btn-primary">
										Pesquisar
									</button>
								</div>
							</div>
						</div>							
					</div>
				</div>
			</section>
		</div>

		<!-- RESULTADOS -->
		<div class="row">
			<section class="col-md-9 col-md-push-1 ordenar-box">
				<ul class="nav navbar-nav">
					<li><h5 class="Ordenar-por">Ordenar por: </h5></li>
					<li class="dropdown">
						<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<a class="a-principal">Mais recente</a>
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
						<li><a href="#">Mais antigas</a></li>
						</ul>
					</li>
				</ul>
				<br>
			</section>
		</div>

		<!-- CONTEÚDO E NOTÍCIAS -->
		<div class="row">
			<section class="col-md-9 col-md-push-1">
				<div class="-resultados-pa">
					<h5>64.576</h5>
					<h5>resultados para</h5>
					<h5>"Música"</h5>
				</div>

				<div class="-resultados" id="result">
					<!-- Local onde ficará disponível a lista de notícias -->
					<!-- result será a id de referência para o arquivo app.js -->
				</div>

			</section>
		</div>

	</section>

	<script src="js/jquery.js"></script>
	<script type="text/javascript" src="js/app.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>